
import { AppLayoutComponent } from './layout/app.layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';






@NgModule({
  imports: [
  
    RouterModule.forRoot([
      { path: '', redirectTo: '/landing', pathMatch: 'full'},
      { path: 'main', component: AppLayoutComponent, 
      children: [
      
          ]
      },
      { path: 'landing', loadChildren: () => import('../app/components/landing/landing.module').then(m => m.LandingModule) },
      { path: 'auth', loadChildren: () => import('../app/components/auth/auth.module').then(m => m.AuthModule) },
    ]),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
