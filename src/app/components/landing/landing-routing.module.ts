import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LandingComponent } from './landing.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: LandingComponent },
        { path: 'auth', loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule) }

    ])],
    exports: [RouterModule]
})
export class LandingRoutingModule { }
